require 'json'
require 'roo'

def lambda_handler(event:, context:)
  { statusCode: 200, body: JSON.generate('Go Serverless v1.0! Your function executed successfully!') }
end

# 安全に文字列を取得するメソッドを追加(拡張)
class Roo::Excelx::Cell::Base
  def text
    return "" if self.value.nil?
    return CGI::unescapeHTML(self.to_s.gsub(/<\/?[^>]*>/,"")) # htmlの場合 タグを除外
  end
end

class ExcelToModelConverter
  def self.execute(xls_path, out_dir)
    puts "load start path:#{xls_path}"
    x = Roo::Excelx.new(xls_path)
    p x.info

      # get sheets
      x.sheets.each_with_index do |sheet_name,i|
        if sheet_name.start_with?("Table")
          execute_table_sheet(sheet_name, x.sheet(sheet_name))
        end
      end


    end

  def self.execute_table_sheet(name, s)
    puts "= execute_table_sheet:#{name}"
    td = nil
    s.each_row_streaming(pad_cells: true) do |row|
      # p "== row=#{row}"
      c1 = row[1]
      c2 = row[2]

      # p "c1.class=#{c1.class} c1.value=#{c1.value} c1.cell_value=#{c1.cell_value}"

      if !c1.value.nil?
        # puts "c1.text=#{c1.text}"
        # puts "c1.cell_value1=#{c1.cell_value}"
        # puts "c1.value1=#{c1.value}"
        # puts "c1.cell_value.to_s1=#{c1.cell_value.to_s}"
        # puts "c1.value.to_s1=#{c1.value.to_s}"
        # puts "c1.to_s1=#{c1.to_s}"
        # puts "c1.style1=#{c1.style}"
        # puts "c1.coordinate1=#{c1.coordinate}"
        if c1.text.start_with?("(") && /\((.+)\)(.+)\((.+)\)/ =~ c1.text
          td = TableDef.new(c1, $1, $2, $3)
        elsif c1.text != "項目"
        # puts "c2.value1=#{c1.value}"
          td.add_column(row)
        end
      end

      if c2.to_s == "" || c2.to_s == "updated_at"
        td.export
      end
    end
  end

end

# コマンドライン実行方法
#  ruby lambda_function.rb /Users/akihikomonden/tsunagaru-docs5/10.設計書/4.DB設計/mgr2/【ビジコネ】テーブル設計_TableAllList\(mgr2\)_SJ_salescomm.xlsx "/tmp"
class TableDef
  attr_accessor :table_no
  attr_accessor :table_name_wa
  attr_accessor :table_name_en
  attr_accessor :rows

  def initialize(cell, no, name_wa, name_en)
    @table_no = no
    @table_name_wa = name_wa
    @table_name_en = name_en
    @rows = []
    @index_names = []
    @index_attrs = {}
    @index_uniq = {}

    puts "* created TableDef / table_no=#{@table_no} table_name_wa=#{@table_name_wa} table_name_en=#{@table_name_en}"
  end

  def add_column(row)
    @rows.push(row)

    # add index if not exist name
    index_name = row[8].text
    # p "index_name=#{index_name}"
    unless index_name == ""
      @index_names.push(index_name) unless @index_names.include?(index_name)
      attrs = @index_attrs[index_name]
      attrs = [] if attrs.nil?
      attrs << row[2].text.to_sym
      @index_attrs[index_name] = attrs
      @index_uniq[index_name] = !row[7].nil? && row[7].text != ""
      # p "@index_names=#{@index_names}"
      # p "@index_attrs=#{@index_attrs}"
      # p "@index_uniq=#{@index_uniq}"
    end
  end

  def export
    puts "[EXPORT START] (#{@table_no})#{@table_name_wa}(#{@table_name_en})"
    puts "  def change"
    puts "    create_table :#{@table_name_en} do |t|"

    @rows.each_with_index do |r,i|
      col_cell = r[2]
      type_cell = r[3]
      null_flg = r[5].text == "" # blankはnull許可
      default_val = r[6].text == "NULL" ? "nil" : r[6].text
      default_str = default_val == "System" || default_val == "" ? "" : ", default: #{default_val}"
      comment = "#{r[1].text} #{r[9].nil? ? "" : r[9].text.gsub(/[\r\n]/,"")}"
      limit_str = ""
      limit_str = ", limit: #{$1}" if /.+\((.+)\)/ =~ type_cell.text
      limit_str = ", limit: #{limit(type_cell.text)}" if limit_str == "" && limit(type_cell.text) != ""
      next if col_cell.text == "id" # rails はidを出力しない
      next if col_cell.text == "created_at" # rails はt.timestampsでまとめて出力
      if col_cell.text == "updated_at" # rails はt.timestampsでまとめて出力
        puts "      t.timestamps"
        next
      end
      puts "      t.#{mysqlToRailsCol(type_cell.text)} :#{col_cell.text}#{limit_str}, null: #{null_flg}#{default_str}, comment: '#{comment}'"
    end

    puts "    end" # create_table

    # append index
    @index_names.each_with_index do |index_name,i|
      cols = @index_attrs[index_name]
      puts "    add_index :#{@table_name_en}, #{cols}, name: '#{index_name}', unique: #{@index_uniq[index_name]}"
    end

    puts "  end" # def change
    puts "[EXPORT END] "
  end

  # from mysql to col
  def mysqlToRailsCol(mysql_col)
    map = {
      "serial" => "BIGINT",
      "tinyint" => "integer",
      "int" => "integer",
      "varchar" => "string",
      "double" => "float",
    }
    if /(.+)\(.*\)/ =~ mysql_col
      mysql_col = $1
    end

    col = map[mysql_col.downcase]
    return col.nil? ? mysql_col.downcase : col.downcase
  end

  # from mysql type to limit
  def limit(mysql_col)
    map = {
      "tinyint" => 1,
      "bigint" => 8,
      "double" => 53,
    }
    map[mysql_col.downcase].to_s
  end
end

# 実行方法
# $ ruby lambda_function.rb xlsx_path output_dir
# 例)
# $ ruby lambda_function.rb /Users/akihikomonden/tsunagaru-docs5/10.設計書/4.DB設計/mgr2/【ビジコネ】テーブル設計_TableAllList\(mgr2\)_SJ_salescomm.xlsx "/tmp"
ExcelToModelConverter.execute(ARGV[0], ARGV[1])